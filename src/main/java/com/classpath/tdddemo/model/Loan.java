package com.classpath.tdddemo.model;

public class Loan {

    private  String loanId;
    private  double loanAmount;

    private String customerName;

    private String customerEmail;

    public Loan(String name, String emailAddress, double loanAmount) {
        this.customerName = name;
        this.customerEmail = emailAddress;
    }

    public Loan(String loanId, String customerName, String customerEmail, double loanAmount) {
        this.customerName = customerName;
        this.customerEmail = customerEmail;
        this.loanId = loanId;
        this.loanAmount = loanAmount;
    }

    public String getCustomerName() {
        return this.customerName;
    }

    public String getCustomerEmail() {
        return this.customerEmail;
    }

    @Override
    public String toString() {
        return "Loan{" +
                "customerName='" + customerName + '\'' +
                ", customerEmail='" + customerEmail + '\'' +
                '}';
    }

    public String getLoanId() {
        return  this.loanId;
    }

    public double getLoanAmount() {
        return this.loanAmount;
    }
}