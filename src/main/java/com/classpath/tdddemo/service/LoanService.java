package com.classpath.tdddemo.service;

import com.classpath.tdddemo.model.Loan;
import com.classpath.tdddemo.repository.LoanRepository;
import org.springframework.stereotype.Service;
import java.util.HashSet;
import java.util.Set;

@Service
public class LoanService {

    private final LoanRepository loanRepository;

    public LoanService(LoanRepository loanRepository) {
        this.loanRepository = loanRepository;
    }

    public Loan applyForLoan(Loan loan) {
        if (loan.getLoanAmount() < 50000){
            throw new IllegalArgumentException("Invalid loan amount");
        }
        return this.loanRepository.save(loan);
    }

    public Set<Loan> fetchLoansByCustomerId(long customerId) {
        Set<Loan> loans = new HashSet<>();
        loans = new HashSet<>(this.loanRepository.findAll());
        return loans;
    }

    public Loan fetchLoanByLoanId(long loanId) {
        return this.loanRepository.findById(loanId).orElseThrow(() -> new IllegalArgumentException("invalid loan id"));
    }

    public void deleteLoanByLoanId(long loanId) {
        this.loanRepository.deleteById(loanId);
    }
}