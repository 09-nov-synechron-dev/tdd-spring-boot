package com.classpath.tdddemo.model;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class LoanTests {

    @Test
    public void createLoanTest(){
        Loan loan = new Loan("ramesh", "ramesh@gmail.com", 2_50_000);
        assertNotNull(loan);
    }

    @Test
    public void testGetters(){
        Loan loan = new Loan("ramesh", "ramesh@gmail.com", 2_50_000);
        String customerName = loan.getCustomerName();
        String customerEmail = loan.getCustomerEmail();
        assertNotNull(customerName);
        assertNotNull(customerEmail);

        assertEquals("ramesh", customerName);
        assertEquals("ramesh@gmail.com", customerEmail);
    }

    @Test
    public void testToString(){
        Loan loan = new Loan("ramesh", "ramesh@gmail.com", 2_50_000);
        assertEquals("Loan{customerName='ramesh', customerEmail='ramesh@gmail.com'}", loan.toString());
    }
}