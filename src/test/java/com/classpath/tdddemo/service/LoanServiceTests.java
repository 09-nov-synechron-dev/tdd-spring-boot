package com.classpath.tdddemo.service;

import com.classpath.tdddemo.model.Loan;
import com.classpath.tdddemo.repository.LoanRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class LoanServiceTests {

    @Mock
    private LoanRepository loanRepository;

    @InjectMocks
    private LoanService loanService ;

    @Test
    public void testLoanServiceConstructor(){
        assertNotNull(loanService);
    }

    @Test
    public void testSaveLoan(){
        //setting the expectations
        Loan loan = new Loan("ramesh", "ramesh@gmail.com", 5000);
        //executing the function
        try {
            Loan loanApplication = loanService.applyForLoan(loan);
            fail("This method should throw IllegalArgumentException");
        }catch(IllegalArgumentException exception){
            assertNotNull(exception);
        }
    }

    @Test
    public void testSaveLoanWithValidData(){
        //setting the expectations
        Loan loan = new Loan("ramesh", "ramesh@gmail.com", 250000);
        lenient().when(loanRepository.save(loan)).thenReturn(new Loan(UUID.randomUUID().toString(), "ramesh", "ramesh@gmail.com", 250000));
        //executing the function
       Assertions.assertThrows(IllegalArgumentException.class, () -> loanService.applyForLoan(loan));
       verify(loanRepository, never()).save(loan);
    }
    @Test
    public void testFetchAllLoansByCustomerId(){

        //set the expectations
        when(loanRepository.findAll()). thenReturn(Arrays.asList(
                new Loan(UUID.randomUUID().toString(), "ramesh", "ramesh@gmail.com", 250000),
                new Loan(UUID.randomUUID().toString(), "suresh", "suresh@gmail.com", 250000),
                new Loan(UUID.randomUUID().toString(), "vinay", "vinay@gmail.com", 250000),
                new Loan(UUID.randomUUID().toString(), "jeevan", "jeevan@gmail.com", 250000),
                new Loan(UUID.randomUUID().toString(), "kiran", "kiran@gmail.com", 250000)
        ));
        Set<Loan> loansByCustomerId = loanService.fetchLoansByCustomerId(12L);
        assertNotNull(loansByCustomerId);
        assertEquals(5, loansByCustomerId.size());

        //verify the expectations that is set
        verify(loanRepository, times(1)).findAll();
    }

    @Test
    public void testFetchLoanByInvalidLoanId(){
        //set the expectations
        when(loanRepository.findById(anyLong())).thenReturn(Optional.empty());

        try {
            loanService.fetchLoanByLoanId(13L);
            fail("This test case should throw IllegalArgument exception");
        } catch (IllegalArgumentException exception){
                assertNotNull(exception);
                assertEquals("invalid loan id", exception.getMessage());
        }
        //verify
        verify(loanRepository, times(1)).findById(13L);
    }

    @Test
    public void testFetchLoanByInvalidLoanIdAndException(){
        //set the expectations
        when(loanRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(IllegalArgumentException.class, () -> loanService.fetchLoanByLoanId(13L));

        //verify
        verify(loanRepository, times(1)).findById(13L);
    }

    @Test
    public void testFetchLoanByValidLoanId(){
        //set the expectations
        when(loanRepository.findById(anyLong())).thenReturn(Optional.of(new Loan(UUID.randomUUID().toString(), "ramesh", "ramesh@gmail.com", 250000)));
        try {
            final Loan loan = loanService.fetchLoanByLoanId(13L);
            assertNotNull(loan);
        }catch (IllegalArgumentException exception){
            fail("This test case is not bound to throw exception");
        }
        //verify
        verify(loanRepository, times(1)).findById(13L);
    }

    @Test
    public void testFetchLoanByValidLoanAccountId(){
        //set the expectations
        when(loanRepository.findById(anyLong())).thenReturn(Optional.of(new Loan(UUID.randomUUID().toString(), "ramesh", "ramesh@gmail.com", 250000)));

            Assertions.assertDoesNotThrow(() -> loanService.fetchLoanByLoanId(13L));
        //verify
        verify(loanRepository, times(1)).findById(13L);
    }

    @Test
    public void deleteLoanByLoanId(){
        doNothing().when(loanRepository).deleteById(anyLong());
        loanService.deleteLoanByLoanId(15L);
        verify(loanRepository, times(1)).deleteById(15L);
    }
}